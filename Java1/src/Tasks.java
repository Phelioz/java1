import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;


public class Tasks {
	Scanner inputScanner;
	String inputString;
	String inputString2;
	String inputString3;
	String inputString4;

	public Tasks(){
		inputScanner = new Scanner(System.in);
		
		// Kollar om talet �r j�mnt (�vning 1)
		exercise1();
		
		// Visar dem fyra vanliga basic operations med talen (�vning 2)
		exercise2();
		
		// Trycker ihop 2-4 str�ngar beronde p� vad du v�ljer (�vning 3)
		exercise3();
		
		/* Checkar hur m�nga g�nger bokst�verna uppst�r 
		 * totalt mellan dem olika str�ngarna 
		 * (�vning 4a)
		*/
		exercise4a();
		
		/* G�r om str�ngarna till ASCII d�r varje ord blir en egen 
		 * array med plats f�r varje bokstav i ASCII 
		 */
		exercise4b();
		
		// Skriver ut n�r n�sta t�g g�r (false tid = 12.25) (true tid = egen input)
		exercise5();
	}

	// Kollar om talet �r j�mnt
	private void exercise1() {
		System.out.println("Uppgift 1");
		System.out.print("Skriv in ett nummer f�r att check om det �r heltal: ");
		inputString = inputScanner.next();
		System.out.println(even(Integer.parseInt(inputString)));
	}
	private boolean even(int num) {
		if (num % 2 == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	//----------------------------------------------------------
	
	// Visar dem fyra vanliga basic operations med talen
	private void exercise2() {
		System.out.println("\nUppgift 2");
		System.out.print("Skriv in f�rsta nummret: ");
		int a = inputScanner.nextInt();
		System.out.print("Skriv in andra nummret: ");
		int b = inputScanner.nextInt();
		mathDisplay(a, b);
	}
	public void mathDisplay(int a, int b){
		System.out.println(a + " + " + b + " = " + (a+b));
		System.out.println(a + " - " + b + " = " + (a-b));
		System.out.println(a + " * " + b + " = " + (a*b));
		System.out.println(a + " / " + b + " = " + ((float)a/b));
	}
	//----------------------------------------------------------
	
	// Trycker ihop 2-4 str�ngar beronde p� vad du v�ljer (�vning 3)
	private void exercise3() {
		System.out.println("\nUppgift 3");
		System.out.print("Hur m�nga texter vill du kombinera?: ");
		inputString = inputScanner.next();
		switch (inputString) {
		case "4":
			System.out.println(strCombiner(inputScanner.next(), inputScanner.next(), inputScanner.next(), inputScanner.next()));
			break;
			
		case "3":
			System.out.println(strCombiner(inputScanner.next(), inputScanner.next(), inputScanner.next()));
			break;
			
		case "2":
			System.out.println(strCombiner(inputScanner.next(), inputScanner.next()));
			break;
			
		default:
			System.out.println("Finns inget st�d f�r antalet du ville kombinera");
			break;
		}
	}
	private String strCombiner(String a, String b){
		String strs[] = {a,b};
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			strBuilder.append(strs[i]);
			if(i - 1 < strs.length){
				strBuilder.append(" ");
			}
		}
		return strBuilder.toString();
	}
	private String strCombiner(String a, String b, String c){
		String strs[] = {a,b,c};
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			strBuilder.append(strs[i]);
			if(i - 1 < strs.length){
				strBuilder.append(" ");
			}
			
		}
		return strBuilder.toString();
	}
	private String strCombiner(String a, String b, String c, String d){
		String strs[] = {a,b,c,d};
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			strBuilder.append(strs[i]);
			if(i - 1 < strs.length){
				strBuilder.append(" ");
			}
		}
		return strBuilder.toString();
	}
	//----------------------------------------------------------
	
	/* Checkar hur m�nga g�nger bokst�verna uppst�r 
	 * totalt mellan dem olika str�ngarna 
	*/
	private void exercise4a() {
		System.out.println("\nUppgift 4a");
		//String test[] = {"Sand", "Landskap", "M�lning"};
		System.out.println("Skriv in tre ord f�r att se det totala nummer f�r hur m�nga g�nger bokst�verna uppst�r");
		String test[] = {inputScanner.next(), inputScanner.next(), inputScanner.next()};
		HashMap<String, Integer> collection = similarChar(test);
		System.out.println("Hur m�nga g�nger bokst�verna uppst�r:");
		printHashMap(collection);
	}
	private HashMap<String, Integer> similarChar(String inArr[]){
		HashMap<String, Integer> collector = new HashMap<String, Integer>();
		for (String string : inArr) {
			for (int i = 0; i < string.length(); i++) {
				String key = Character.toString(string.charAt(i)).toUpperCase();
				
				if (collector.containsKey(key)) {
					int value = collector.get(key);
					collector.put(key, value + 1);
				}
				else {	
					collector.put(key, 1);
				}
			}
		}	
		return collector;
	}
	private void printHashMap(HashMap<String, Integer> collection){
		Iterator<String> iterator = collection.keySet().iterator();
			while(iterator.hasNext()) {
				String temp = iterator.next();
				System.out.println(temp + " = " + collection.get(temp));
			}
		
	}
	//----------------------------------------------------------
	
	/* G�r om str�ngarna till ASCII d�r varje ord blir en egen 
	 * array med plats f�r varje bokstav i ASCII 
	 */
	private void exercise4b() {
		System.out.println("\nUppgift 4b");
		//String test[] = {"Sand", "Landskap", "M�lning"};
		System.out.println("Skriv in tre ord f�r att se alla deras bokst�ver i ASCII");
		String test[] = {inputScanner.next(), inputScanner.next(), inputScanner.next()};
		ArrayList<ArrayList<String>> arrayList = stringsToASCII(test);
		printArrayList(arrayList);
	}
	private ArrayList<ArrayList<String>> stringsToASCII(String input[]){
		
		ArrayList<ArrayList<String>> arrayList = new ArrayList<ArrayList<String>>();
		
		for (String string : input) {
			ArrayList<String> strList = new ArrayList<String>();
			for (char c : string.toCharArray()) {
				strList.add(Integer.toString((int)c));
			}
			arrayList.add(strList);
		}
		
		return arrayList;
	}
	private void printArrayList(ArrayList<ArrayList<String>> inputArrayList)
	{
		int count = 0;
		for (ArrayList<String> arrayList : inputArrayList) {
			System.out.println("Word " + ++count);
			System.out.println("-------------------------");
			for (String string : arrayList) {
				System.out.println(string);
			}
			System.out.println("-------------------------\n");
		}
	}
	//----------------------------------------------------------
	
	// Skriver ut n�r n�sta t�g g�r 
	private void exercise5() {
		System.out.println("\nUppgift 5");
	
		//float time = 12.25f;
		System.out.print("Skriv in klockslaget: ");
		float time = inputScanner.nextFloat();
		System.out.println(getNextTrainTime(time));
	}
	private float getNextTrainTime(float curTime) {
		// F�r ut timmen bara
		int curTimeInt = (int) curTime;
		// F�r ut minutslaget bara
		float curMin = curTime - curTimeInt;
		
		// T�g tiderna
		float trainTime1 = 0.25f;
		float trainTime2 = 0.45f;
		
		if (curMin < trainTime1) {
			return curTimeInt + trainTime1;
		}
		else if (curMin < trainTime2) {
			return curTimeInt + trainTime2;
		}
		else {
			return ((curTimeInt + 1) % 24) + trainTime1;
		}
		
	}
	//----------------------------------------------------------
}
