import java.util.Scanner;


public class Paint {
	static Scanner inputScanner;
	static String inputString;
	static float paintReqLiter;
	
	static float paintLiterFillSqrs;
	static float literPerBucket;
	
	static float wallSizeSqr = 0;
	public Paint()
	{
		paintReqLiter = 0f;
		inputScanner = new Scanner(System.in);
		
		setPaintLiterFill();
		setLitersPerBucket();

		
		System.out.print("�r v�ggarna du ska m�la i samma storlek?");
		inputString = inputScanner.next();
		
		if (inputString.equalsIgnoreCase("ja")) {
			System.out.print("Hur m�nga v�gar vill du m�la: ");
			inputString = inputScanner.next();
			setWallSize();
			
			
			int wallsToPaint = Integer.parseInt(inputString);
			for (int i = 0; i < wallsToPaint; i++) {
				paintReqLiter += paintReqMulti();
			}
			
			System.out.println("Vill du ber�kna bort n�got fr�n v�ggarna? tex en d�rr (ja/nej)");
			inputString = inputScanner.next();
			
			// Checkar om du vill sk�ra ut n�got fr�n ber�kningen
			if (inputString.equalsIgnoreCase("ja") || inputString.equalsIgnoreCase("yes")) {
				System.out.println("Skriv in bredden p� vad du vill sk�ra ut fr�n ber�kningen: ");
				inputString = inputScanner.next();
				float cutX = Float.parseFloat(inputString);
				
				System.out.println("Skriv in h�jden p� vad du vill sk�ra ut fr�n ber�kningen: ");
				inputString = inputScanner.next();
				float cutY = Float.parseFloat(inputString);
				for (int i = 0; i < wallsToPaint; i++) {
					cutOutMulti(cutX, cutY);
				}
			}
		}
		else {
			while (true) {
				setWallSize();
				
				paintReq();
				System.out.println("Vill du l�gga till en till v�gg? (ja/nej)");
				inputString = inputScanner.next();
				if (inputString.equalsIgnoreCase("nej")) {
					break;
				}
			}
		}
		printLiterAndBucket();
	}
	private static void printLiterAndBucket() {
		System.out.println("_______________________________________\n");
		System.out.println(paintReqLiter + " liter f�rg beh�ver du");
		System.out.println("Vilket �r " + literToBucket() + " burkar");
	}
	
	// Sets wall size in sqr meters
	private static void setWallSize() {
		System.out.println("Skriv in bredden p� v�ggen: ");
		String wallX = inputScanner.next();
		float fWallX = Float.parseFloat(wallX);
		System.out.println("Skriv in bredden p� h�jden: ");
		String wallY = inputScanner.next();
		float fWallY = Float.parseFloat(wallY);
		
		wallSizeSqr = fWallX * fWallY;
	}
	
	// Sets how many sqr meters one liter of paint covers
	private static void setPaintLiterFill() {
		System.out.print("Hur hur m�nga kvadrat meter t�cker en liter f�rg: ");
		inputString = inputScanner.next();
		paintLiterFillSqrs = Float.parseFloat(inputString);
	}
	
	// Sets how many liters one bucket holds 
	private static void setLitersPerBucket() {
		System.out.print("Hur hur mycket liter rymmer en burk med f�rg: ");
		inputString = inputScanner.next();
		literPerBucket = Float.parseFloat(inputString);
	}
	
	// Converts liters to buckets
	public static int literToBucket()
	{
		int buckets;
		
		buckets = Math.round(paintReqLiter / literPerBucket);
		
		return buckets;
		
	}
	
	// Sets how many liters of paint you will need
	public static void paintReq()
	{
		// F�r fram hur mycket liter f�rg du beh�ver
		paintReqLiter = wallSizeSqr / paintLiterFillSqrs;
		
		System.out.println("Vill du ber�kna bort n�got fr�n v�ggen? tex en d�rr (ja/nej)");
		inputString = inputScanner.next();
		
		// Checkar om du vill sk�ra ut n�got fr�n ber�kningen
		if (inputString.equalsIgnoreCase("ja") || inputString.equalsIgnoreCase("yes")) {
			cutOut();
		}
	}
	
	public static float paintReqMulti()
	{
		// Init
		float paintLitersReq = 0;
		
		// F�r fram hur mycket liter f�rg du beh�ver
		paintLitersReq = wallSizeSqr / paintLiterFillSqrs;
	
		return paintLitersReq;
	}
	
	private static void cutOut() {
		System.out.println("Skriv in bredden p� vad du vill sk�ra ut fr�n ber�kningen: ");
		inputString = inputScanner.next();
		float cutX = Float.parseFloat(inputString);
		
		System.out.println("Skriv in h�jden p� vad du vill sk�ra ut fr�n ber�kningen: ");
		inputString = inputScanner.next();
		float cutY = Float.parseFloat(inputString);
		
		float cutSqr = cutX * cutY;
		paintReqLiter -= cutSqr / paintLiterFillSqrs;
		
	}
	
	private static void cutOutMulti(float cutX, float cutY) {
		
		float cutSqr = cutX * cutY;
		paintReqLiter -= cutSqr / paintLiterFillSqrs;
		
	}
}
